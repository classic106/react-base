import { PreloaderStyle } from './styles/Preloader';

const Preloader = () => {
  return (
    <PreloaderStyle className="preloader">
      <div></div>
      <div></div>
    </PreloaderStyle>
  );
};

export { Preloader };
