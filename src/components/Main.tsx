import React, { useReducer } from 'react';

import { IReductMessage } from '../types';
import { reducer } from '../state/reducer';
import { ReductMessageContext } from '../state/context';

import { Chat } from './Chat';

const InitialState = (initialVlaues: IReductMessage) => {
  return { ...initialVlaues };
};

function Main() {
  const [state, dispath] = useReducer(
    reducer,
    { messageId: '', text: '', edit: false },
    InitialState
  );

  return (
    <ReductMessageContext.Provider value={{ state, dispath }}>
      <Chat />
    </ReductMessageContext.Provider>
  );
}

export { Main };
