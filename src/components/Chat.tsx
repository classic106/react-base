import React, { FC, useEffect, useState, useContext } from 'react';

import { ChatStyle } from './styles/ChatStyle';

import { IMessage } from '../types';
import { ReductMessageContext } from '../state/context';
import { getMessages } from '../api';

import { Header } from './Header';
import { MessagesList } from './MessagesList';
import { MessageInput } from './MessageInput ';

const Chat: FC = () => {
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [users, setUsers] = useState<string[]>([]);
  const { state, dispath } = useContext(ReductMessageContext);
  const { text, messageId, edit } = state;

  useEffect(() => {
    if (edit) {
      const newMessage = messages.map((message) => {
        if (message.id === messageId) {
          message.text = text;
        }
        return message;
      });

      dispath({ type: 'RESET', payload: '' });
      setMessages(newMessage);
    }
  }, [text, messageId, edit]);

  const getLastMessage = () => {
    if (messages.length) {
      return messages[messages.length - 1].createdAt;
    }
    return null;
  };

  useEffect(() => {
    localStorage.setItem('myId', '9e243930-83c9-11e9-8e0c-8f1a686f4ce4');

    async function getData() {
      const data = await getMessages();
      const users = new Set<string>();

      data.forEach((el) => users.add(el.user));
      setMessages(data);
      setUsers(Array.from(users));
    }
    getData();
  }, []);

  return (
    <ChatStyle className="chat">
      <Header
        participantsCount={users.length}
        messagesCount={messages.length}
        lastMessgeDate={getLastMessage()}
      />
      <MessagesList messages={messages} />
      <MessageInput />
    </ChatStyle>
  );
};

export { Chat };
