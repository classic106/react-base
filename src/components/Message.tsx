import React, { FC } from 'react';

import { IMessage as MessageProps } from '../types';
import { MessageStyle } from './styles/MessageStyle';

import { parseDate } from '../helpers';

interface IMessage {
  message: MessageProps;
}

const Message: FC<IMessage> = ({ message }) => {
  return (
    <MessageStyle className="message">
      <div className="message_left_side">
        <div className="image_wrap">
          <img src={message.avatar} alt={message.user} />
        </div>
        <p className="messge_user">{message.user}</p>
      </div>
      <div className="left_main">
        <p className="message_text">{message.text}</p>
        <div className="left_main_bottom">
          <p className="message_time">{parseDate(message.createdAt)}</p>
          <button className="message-like button">Like</button>
        </div>
      </div>
    </MessageStyle>
  );
};

export { Message };
