import React, { FC, useState, ChangeEvent, useContext, useEffect } from 'react';

import { MessageInputStyle } from './styles/MessageInputStyle';
import { ReductMessageContext } from '../state/context';

const MessageInput: FC = () => {
  const { state, dispath } = useContext(ReductMessageContext);
  const { text, messageId } = state;
  const [messageText, setMessageText] = useState<string>('');

  useEffect(() => setMessageText(text), [messageId, text]);

  const onChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    const { value } = e.target;
    if (messageId) {
      dispath({ type: 'SET_TEXT', payload: value });
    } else {
      setMessageText(value);
    }
  };

  const onClick = () => {
    if (messageId) {
      dispath({ type: 'EDIT', payload: messageText });
      setMessageText('');
    } else {
      if (messageText) {
        //send to server
        console.log(messageText);
      } else {
        alert('Empty field!!!');
      }
    }
  };

  return (
    <MessageInputStyle className="message_input">
      <textarea
        onChange={onChange}
        className={
          messageId ? 'message_input_text focus' : 'message_input_text'
        }
        placeholder="Enter your message..."
        value={messageId ? text : messageText}
      ></textarea>
      <button className="message_input_button" onClick={onClick}>
        {messageId ? 'EDIT' : 'SEND'}
      </button>
    </MessageInputStyle>
  );
};

export { MessageInput };
