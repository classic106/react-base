import React, { FC } from 'react';

import { HeaderStyle } from './styles/HeaderStyle';
import { parseDate } from '../helpers';

interface IHeader {
  participantsCount: number;
  messagesCount: number;
  lastMessgeDate: string | null;
}

const Header: FC<IHeader> = ({
  participantsCount,
  messagesCount,
  lastMessgeDate
}) => {
  return (
    <HeaderStyle className="header">
      <div className="header_title">MyChat</div>
      <div className="header_content">
        <div className="header_content_center">
          <p className="header_users_count">{participantsCount} participants</p>
          <p className="header_messages_count">{messagesCount} messages</p>
        </div>
        <p className="header_last_message_date">
          last messge at {parseDate(lastMessgeDate as string)}
        </p>
      </div>
    </HeaderStyle>
  );
};

export { Header };
