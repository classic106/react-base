import React, { FC, useState, useContext } from 'react';

import { IMessage as MessageProps } from '../types';
import { OwnMessageStyle } from './styles/OwnMessageStyle';

import { ReductMessageContext } from '../state/context';
import { parseDate } from '../helpers';

interface IMessage {
  message: MessageProps;
}

const OwnMessage: FC<IMessage> = ({ message }) => {
  const [edit, setEdit] = useState<boolean>(true);
  const { dispath } = useContext(ReductMessageContext);

  const onClick = () => {
    setEdit(!edit);

    if (edit) {
      dispath({ type: 'SET_MASSAGE_ID', payload: message.id });
      dispath({ type: 'SET_TEXT', payload: message.text });
    } else {
      dispath({ type: 'RESET', payload: '' });
    }
  };

  return (
    <OwnMessageStyle className="own_message">
      <p className="message_text">{message.text}</p>
      <div className="right_bottom">
        <p className="message_time">{parseDate(message.createdAt)}</p>
        <button
          className={edit ? 'message-edit button' : 'message-edit button edit'}
          onClick={onClick}
        >
          {edit ? 'edit' : 'cancel'}
        </button>
        <button className="message-delete button">delete</button>
      </div>
    </OwnMessageStyle>
  );
};

export { OwnMessage };
