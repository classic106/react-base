import React, { FC } from 'react';

import { IMessage } from '../types';
import { MessagesListStyle } from './styles/MessagesStyle';

import { Message } from './Message';
import { Preloader } from './Preloader';
import { OwnMessage } from './OwnMessage';

interface IMessages {
  messages: IMessage[];
}

const MessagesList: FC<IMessages> = ({ messages }) => {
  if (!messages.length) {
    return (
      <MessagesListStyle>
        <Preloader />
      </MessagesListStyle>
    );
  }

  const myId = localStorage.getItem('myId');
  //const date = Date.now();

  return (
    <MessagesListStyle>
      <div className="wrap_message_list">
        {messages.map((message) => {
          //console.log(new Date(date), new Date(message.createdAt));
          if (message.userId === myId) {
            return <OwnMessage message={message} key={message.id} />;
          }
          return <Message message={message} key={message.id} />;
        })}
      </div>
    </MessagesListStyle>
  );
};

export { MessagesList };
