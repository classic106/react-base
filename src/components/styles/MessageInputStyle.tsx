import styled from 'styled-components';

const MessageInputStyle = styled.div`
  & {
    display: flex;
    align-items: center;
    width: 100%;
    background-color: #4a148c;
    position: relative;
  }

  &:before {
    content: '';
    width: 100%;
    height: 1px;
    position: absolute;
    top: -1px;
    background-color: #4a138c;
  }

  .message_input_text {
    max-width: 82%;
    min-width: 82%;
    min-height: 30px;
    max-height: 30px;
    overflow-x: auto;
    background: none;
    color: white;
    border: none;
    padding: 10px;
    font-size: 13px;
    resize: none;
  }

  .message_input_text:focus,
  .message_input_text.focus {
    outline: none !important;
    box-shadow: 0 0 10px #719ece;
  }

  .message_input_button {
    background: none;
    border: none;
    color: white;
    height: 100%;
    width: 100%;
    font-size: 15px;
  }

  .message_input_button:hover {
    background-color: #7c42bd;
  }
`;

export { MessageInputStyle };
