import styled from 'styled-components';

const ChatStyle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100vh;
`;

export { ChatStyle };
