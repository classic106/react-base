import styled from 'styled-components';

const MessagesListStyle = styled.div`
  width: 100%;
  height: calc(100vh - 100px);
  overflow-x: auto;
  paddin: 10px;
  background-color: #7c43bd;

  .wrap_message_list {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    height: 100%;
  }
`;

export { MessagesListStyle };
