import styled from 'styled-components';

const MessageStyle = styled.div`
  .button {
    width: max-content;
    height: max-content;
    border: none;
    color: blue;
    padding: 0;
    padding: 3px 10px;
    background: none;
    border-radius: 2px;
    cursor: pointer;
    width: 56px;
  }

  .message_text {
    width: 100%;
    white-space: pre-wrap;
  }

  .button.message-like:hover {
    background-color: blue;
    color: white;
  }

  .button.edit.message-edit:hover {
    background-color: dimgray;
  }

  .message_time {
    padding: 5px;
    margin: 0;
  }

  &.message {
    width: max-content;
    max-width: 75%;
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 5px;
    margin-bottom: 10px;
    message {
      padding: 5px;
    }
  }

  &.message {
    background-color: gainsboro;
    border-radius: 0px 10px 10px 10px;
    align-self: start;
    margin: 5px 10px;

    .message_left_side {
      display: flex;
      flex-direction: column;
      align-items: center;
      padding: 10px;

      .image_wrap {
        border-radius: 50%;
        width: 50px;
        height: 50px;
        overflow: hidden;
        display: flex;
        justify-content: center;

        img {
          width: 100%;
        }
      }
    }

    .left_main {
      padding: 10px;
      align-items: flex-end;
      display: flex;
      flex-direction: column;

      .left_main_bottom {
        display: flex;
        align-items: center;
        justify-content: space-between;
      }
    }
  }
`;

export { MessageStyle };
