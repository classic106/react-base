import styled from 'styled-components';

const OwnMessageStyle = styled.div`
  .button {
    width: max-content;
    height: max-content;
    border: none;
    color: blue;
    padding: 0;
    padding: 3px 10px;
    background: none;
    border-radius: 2px;
    cursor: pointer;
    width: 56px;
  }

  .message_text {
    width: 100%;
    white-space: pre-wrap;
  }

  .button.edit.message-edit {
    color: lightgray;
    background-color: dimgray;
  }

  .button.message-edit:hover {
    background-color: blue;
    color: white;
  }

  .button.edit.message-edit:hover {
    background-color: dimgray;
  }

  .button.message-delete {
    background-color: #ff4b4b;
    color: white;
  }

  .button.message-delete:hover {
    background-color: red;
  }

  .message_time {
    padding: 5px;
    margin: 0;
  }

  &.own_message {
    width: max-content;
    max-width: 75%;
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 5px;
    background-color: gainsboro;
    border-radius: 0px 10px 10px 10px;
    align-self: start;
    margin: 5px 10px;

    .message {
      padding: 5px;
    }

    .message_left_side {
      display: flex;
      flex-direction: column;
      align-items: center;
      padding: 10px;

      .image_wrap {
        border-radius: 50%;
        width: 50px;
        height: 50px;
        overflow: hidden;
        display: flex;
        justify-content: center;

        img {
          width: 100%;
        }
      }
    }

    .left_main {
      padding: 10px;
      align-items: flex-end;
      display: flex;
      flex-direction: column;

      .left_main_bottom {
        display: flex;
        align-items: center;
        justify-content: space-between;
      }
    }
  }

  &.own_message {
    flex-direction: column;
    margin: 10px 10px;
    align-self: flex-end;
    border-radius: 10px 10px 0px 10px;
    position: relative;
    padding: 10px;
    background-color: darkgray;

    input[type='text'] {
      font-size: 14px;
    }

    input[type='text']:disabled {
      background: none;
      border: none;
      padding: 5px;
    }

    .right_bottom {
      display: flex;
      width: 100%;
      align-items: end;
      justify-content: end;
    }
  }

  &.own_message::before {
    display: block;
    content: 'me';
    top: -10px;
    left: -20px;
    font-size: 0.6em;
    font-weight: bold;
    position: absolute;
    border-radius: 50%;
    padding: 6px;
    background-color: darkgray;
  }
`;

export { OwnMessageStyle };
