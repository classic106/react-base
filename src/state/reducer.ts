import { IReductMessage, IAction } from '../types';

const reducer = (state: IReductMessage, action: IAction) => {
  switch (action.type) {
    case 'SET_TEXT':
      return { ...state, text: action.payload };

    case 'SET_MASSAGE_ID':
      return { ...state, messageId: action.payload };

    case 'EDIT':
      return { ...state, text: action.payload, edit: true };

    case 'RESET':
      return { ...state, messageId: '', text: '', edit: false };

    default:
      return state;
  }
};

export { reducer };
