import { createContext, Dispatch } from 'react';
import { IReductMessage, IAction } from '../types';

const state = { messageId: '', text: '', edit: false };

const ReductMessageContext = createContext<{
  state: IReductMessage;
  dispath: Dispatch<IAction>;
}>({ state, dispath: () => null });

export { ReductMessageContext };
