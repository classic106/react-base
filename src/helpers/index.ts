const parseDate = (messageDate: string) => {
  if (messageDate) {
    const date = new Date(messageDate);
    const hour = date.getHours();
    const minutes = date.getMinutes();
    return `${hour}:${minutes}`;
  }

  return null;
};

export { parseDate };
