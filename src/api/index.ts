import { IMessage } from '../types';

async function getMessages(): Promise<IMessage[]> {
  return fetch(
    'https://edikdolynskyi.github.io/react_sources/messages.json'
  ).then((res) => res.json());
}

export { getMessages };
