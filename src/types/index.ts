interface IMessage {
  avatar: string;
  createdAt: string;
  editedAt: string;
  id: string;
  text: string;
  user: string;
  userId: string;
}

interface IReductMessage {
  messageId: string;
  text: string;
  edit?: boolean;
}

interface IAction {
  type: string;
  payload: string;
}

export type { IMessage, IReductMessage, IAction };
